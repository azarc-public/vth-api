/**
 * @fileoverview gRPC-Web generated client stub for api.v1
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as v1_hello_v1_pb from '../v1/hello_v1_pb';


export class HelloClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoSayHello = new grpcWeb.AbstractClientBase.MethodInfo(
    v1_hello_v1_pb.HelloResponse,
    (request: v1_hello_v1_pb.HelloRequest) => {
      return request.serializeBinary();
    },
    v1_hello_v1_pb.HelloResponse.deserializeBinary
  );

  sayHello(
    request: v1_hello_v1_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null): Promise<v1_hello_v1_pb.HelloResponse>;

  sayHello(
    request: v1_hello_v1_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: v1_hello_v1_pb.HelloResponse) => void): grpcWeb.ClientReadableStream<v1_hello_v1_pb.HelloResponse>;

  sayHello(
    request: v1_hello_v1_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: v1_hello_v1_pb.HelloResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/api.v1.Hello/SayHello',
        request,
        metadata || {},
        this.methodInfoSayHello,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/api.v1.Hello/SayHello',
    request,
    metadata || {},
    this.methodInfoSayHello);
  }

}

